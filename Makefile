.PHONY: clean-pyc clean-build docs clean
define BROWSER_PYSCRIPT
import os, webbrowser, sys
try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT="chromium"
BROWSER := $(PYTHON) -c "$$BROWSER_PYSCRIPT"
PYTHON3 := $(shell which python3)

help:
	@echo "clean - remove all build, test, coverage and Python artifacts"
	@echo "clean-build - remove build artifacts"
	@echo "clean-pyc - remove Python file artifacts"
	@echo "clean-test - remove test and coverage artifacts"
	@echo "lint - check style with flake8"
	@echo "test - run tests quickly with the default Python"
	@echo "test-all - run tests on every Python version with tox"
	@echo "coverage - check code coverage quickly with the default Python"
	@echo "docs - generate Sphinx HTML documentation, including API docs"
	@echo "release - package and upload a release"
	@echo "dist - package"
	@echo "install - install the package to the active Python's site-packages"

clean: clean-build clean-pyc clean-test

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test:
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/

lint: install-tests
	flake8 switchywitchy tests

install-tests:
	pip install -r requirements/tests.txt

install-docs:
	pip install -r requirements/docs.txt

test: install-tests mypy lint
	python setup.py test
	echo "tests completed"

test-all:
	tox

coverage: install-tests
	coverage run --source switchywitchy setup.py test
	coverage report -m
	coverage html

docs: install-docs
	rm -f docs/switchywitchy.rst
	rm -f docs/modules.rst
	sphinx-apidoc -o docs/ switchywitchy
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	$(BROWSER) docs/_build/html/index.html

mypy:
	$(PYTHON3) -m mypy --show-traceback --ignore-missing-import -p switchywitchy


servedocs: docs
	watchmedo shell-command -p '*.rst' -c '$(MAKE) -C docs html' -R -D .

release: clean
	$(PYTHON3) setup.py sdist upload
	$(PYTHON3) setup.py bdist_wheel upload

dist: clean
	$(PYTHON3) setup.py sdist
	$(PYTHON3) setup.py bdist_wheel
	ls -l dist

install: clean
	$(PYTHON3) setup.py install
