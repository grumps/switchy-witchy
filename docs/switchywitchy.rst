switchywitchy package
=====================

Submodules
----------

switchywitchy\.config module
----------------------------

.. automodule:: switchywitchy.config
    :members:
    :undoc-members:
    :show-inheritance:

switchywitchy\.machines module
------------------------------

.. automodule:: switchywitchy.machines
    :members:
    :undoc-members:
    :show-inheritance:

switchywitchy\.models module
----------------------------

.. automodule:: switchywitchy.models
    :members:
    :undoc-members:
    :show-inheritance:

switchywitchy\.switchywitchy module
-----------------------------------

.. automodule:: switchywitchy.switchywitchy
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: switchywitchy
    :members:
    :undoc-members:
    :show-inheritance:
