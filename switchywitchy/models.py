#-*- coding: utf-8 -* # noqa: E265
"""Traps capture varying events within the system"""
__author__ = "Maxwell J. Resnick"
__docformat__ = "reStructuredText"

import logging
import json
from collections import OrderedDict
import psutil
import curio
import arrow
from typing import Dict, Tuple
from .machines import StateMachineMixin

logger = logging.getLogger('switchywitchy')


def _process_parent(current_proc: psutil.Process):
    parent = current_proc.parent()
    try:
        if current_proc.name() != parent.name():
            return current_proc.pid
    except AttributeError:
        # We've traveresed all the way to PID 1,
        # `process.parent()` returns None
        return current_proc.pid
    return _process_parent(current_proc.parent())


def create_watch(properties: Dict):
    """
    finds a process based on properties provided. Creates a Proc for
    the parent process.

    :param namedtuple properties: properies of the process to watch
    :return Proc:
    :rtype Proc:
    """

    def _is_match(proc: psutil.Process, properties: Dict):
        property_fields_match = [properties.get(field) == getattr(
            proc, field)()
            for field in properties.keys()]
        return all(property_fields_match)

    for proc in psutil.process_iter():
        if _is_match(proc, properties):
            # return the parent of this process
            return psutil.Process(pid=_process_parent(proc))


class Trap(StateMachineMixin):
    """
    An alarm or trigger, to be tripped by a process.
    Property handling is as follows:

        - Properties are assigned by default values, but are also overridden based on the configuartion of the application being watched. # noqa: E501
        - The underlying process properties are utilized are set by prefixing the property key with `process_` # noqa: E501
        - Properties for the trap are set by prefixing the keys in configuration with `watch_` # noqa: E501

    """
    PROPERTIES = {
        'watch': {
            'max_cpu_usage': '30',
            'max_memory': '60',
            'heartbeat_interval': '5',
            'lower_control': '10',
            'upper_control':  '10'}
    }

    def __init__(self, properties, app_queue, state=None):
        # sets default attributes
        self.state = state
        self.process = None
        for key, value in self.PROPERTIES["watch"].items():
            setattr(self, key, value)
        self.properties = self.handle_properties(properties)
        self.memory_stats = OrderedDict()
        self.cpu_stats = OrderedDict()
        self.queue = curio.Queue()
        self.app_queue = app_queue
        self.setup_init_state()

    def handle_properties(self, properties):
        """
        pops off the default trap properties, setting
        them to this instance. This is before it is passed
        down to the :class:`Proc`

        :param dict properties: dictionary of properties
        :returns: properties sans trap properties
        :rtype: dict
        """
        handled_properties = {}
        for key in properties.keys():
            property_type, property_key = key.split("_", maxsplit=1)
            try:
                if property_key in self.PROPERTIES[property_type]:
                    setattr(self, property_key, properties[key])
            except KeyError:
                if property_type == "process":
                    handled_properties.update(
                        {property_key: properties[key]})
        return handled_properties

    def check_status(self, threshold=None, current_value=None):
        """
        compare threshold value to, current value
        fail if current value
        """
        if threshold < current_value:
            return "FAIL"
        return "PASS"

    async def check_cpu(self):
        """
        checks cpu, emits check to queue
        """
        # TODO needs to be a subprocess
        current_cpu = self.process.cpu_percent(interval=1)
        current_time = arrow.utcnow().timestamp
        status = self.check_status(
            threshold=self.max_cpu_usage, current_value=current_cpu)
        logger.debug("{} check cpu status is: {}".format(current_time, status))
        self.cpu_stats[current_time] = (current_cpu, status)
        message = Message.create(
            ("cpu_utilization", current_time, self.cpu_stats))
        await self.queue.put(message)

    async def check_memory(self):
        """
        checks memory, emits status to queue
        """
        # TODO needs to be a subprocess
        current_memory = self.process.memory_percent()
        current_time = arrow.utcnow().timestamp
        status = self.check_status(
            threshold=self.max_memory, current_value=current_memory)
        self.memory_stats[current_time] = (current_memory, status)
        message = Message.create(("memory_utilization",
                                  current_time,
                                  self.memory_stats))
        logger.debug("{} check memory status is: {}".format(
            current_time, status))
        await self.queue.put(message)

    async def check(self):
        """
        """
        memory_task = await curio.spawn(self.check_memory())
        cpu_task = await curio.spawn(self.check_cpu())
        return memory_task, cpu_task

    def __repr__(self):
        try:
            return "Trap for {} {}".format(self.process.name(),
                                           self.state)
        except AttributeError:
            return "Unbound Trap"

    def __str__(self):
        try:
            return "Trap for {} {}".format(self.process.name(),
                                           self.state)
        except AttributeError:
            return "Unbound Trap"


class Message(object):
    """
    A message.

    :param dict data: internal datatype OrderedDict
    :param str sender:
    :param timestamp: sets the timestamp based on data, else when object is contructed # noqa E501
    .. py:attribute:: _initial message obj before encode, internal use

    """

    def __init__(self, data=None, sender=None, timestamp=None):
        self._initial = {}
        self.is_valid(data=data, sender=sender)
        self.created_timestamp = self.create_timestamp()
        self.timestamp = timestamp or self.created_timestamp
        self._initial.update(
            {'timestamp': self.timestamp,
             'data_timestamp': self.timestamp,
             'data': data,
             'sender': sender}
        )

    def is_valid(self, data=None, sender=None):
        """
        checks for a valid message, sets .initial
        TBD actually message format

        :returns: boolean
        :rtype: bool

        """
        if not isinstance(data, dict):
            raise NameError("data: is not defined")
        if not isinstance(sender, str):
            raise NameError("sender: is not defined")
        self.sender = sender
        self.data = data

    def results(self) -> str:
        """
        results for this message
        """
        return self.data[self.timestamp]

    def create_timestamp(self):
        """
        creation timestamp, utc

        :return: arrow.utcnow()
        :rtype: object
        """
        return arrow.utcnow().timestamp

        # isoformat(sep="T")

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "Message: {}, {}".format(self.timestamp, self.data)

    def encode(self):
        """JSON encodes .initial

        :returns: json.dump
        :rtype: bytes
        """
        return json.dumps(self._initial)

    @classmethod
    def create(cls, data: Tuple[str, arrow.Arrow, Dict]):
        """factory for creating an object"""
        sender = data[0]
        timestamp = data[1]
        result_data = data[2]
        return cls(result_data, sender, timestamp)
